/*
    This file is part of the OpenCSA Project under the MIT License
*/
#include "lexer.h"
#include "token.h"

extern inline char *tok_get_text(token_t *tok);

int main(int argc, char **argv) {
    // For now, argv[1] will be the file path
    FILE *file = fopen(argv[1], "r");
    fseek(file, 0, SEEK_END);
    long file_len = ftell(file);
    fseek(file, 0, SEEK_SET);
    char *buffer = malloc(file_len + 1);
    buffer[file_len] = 0;
    fread(buffer, 1, file_len, file);
    fclose(file);

    lexer_t lexer = (lexer_t){.file_path = argv[1],
                              .file_length = file_len,
                              .line = 1,
                              .col = 1,
                              .failed = false,
                              .ptr = buffer,
                              .end_ptr = &buffer[file_len]};

    token_t tok;
    lex_tok(&lexer, &tok);
    while (tok.kind != file_end) {
        printf("token: %d\n", tok.kind);
        if(tok.kind == double_literal) {
            char *text = tok_get_text(&tok);
            puts(text);
            free(text);
        }
        lex_tok(&lexer, &tok);
    }

    free(buffer);
}