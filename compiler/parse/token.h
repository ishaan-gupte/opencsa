/*
    This file is part of the OpenCSA Project under the MIT License
*/
#ifndef OPENCSA_TOKEN_H
#define OPENCSA_TOKEN_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum tokenkind {
    file_end,

    // Separators
    l_paren,
    r_paren,
    l_curly,
    r_curly,
    l_square,
    r_square,
    semicolon,
    comma,
    dot,
    
    // Operators
    plus,
    minus,
    asterisk,
    slash,
    percent,
    plus_plus,
    minus_minus,
    equals,
    plus_equals,
    minus_equals,
    asterisk_equals,
    slash_equals,
    percent_equals,
    equals_equals,
    exclaimation_equals,
    less,
    less_equals,
    greater,
    greater_equals,
    exclaimation,
    ampersand_ampersand,
    pipe_pipe,

    // literals
    int_literal,
    double_literal,
    string_literal,
    identifier,

    // keywords
    true_keyword,
    false_keyword,
    int_keyword,
    double_keyword,
    boolean_keyword,
    if_keyword,
    else_keyword,
    while_keyword,
    for_keyword,
    return_keyword,
    public_keyword,
    private_keyword,
    static_keyword,
    final_keyword,
    super_keyword,
    this_keyword,
    abstract_keyword,
    class_keyword,
    interface_keyword,
    import_keyword,
    null_keyword
} tokenkind_t;

typedef struct token {
    tokenkind_t kind;
    int line, col;
    char *text;
    int len;
} token_t;

inline void tok_set_symbol(token_t *tok, tokenkind_t kind, int line, int col) {
    tok->kind = kind;
    tok->line = line;
    tok->col = col;
}

inline void tok_set_literal(token_t *tok, tokenkind_t kind, int line, int col,
                            char *text, int len) {
    tok->kind = kind;
    tok->line = line;
    tok->col = col;
    tok->text = text;
    tok->len = len;
}

inline char *tok_get_text(token_t *tok) {
    char *text = malloc(tok->len + 1);
    if (!text) {
        fprintf(stderr, "fatal error: out of memory\n");
        return NULL;
    }

    // Set the null terminator
    text[tok->len] = 0;

    // Copy the text for the string
    memcpy(text, tok->text, tok->len);

    return text;
}

#endif