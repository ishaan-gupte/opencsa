/* C code produced by gperf version 3.1 */
/* Command-line: /Users/3009876/Downloads/gperf-3.1/src/gperf --output-file=keywords.c keywords.gperf  */
/* Computed positions: -k'2' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gperf@gnu.org>."
#endif

#line 7 "keywords.gperf"

    #include <stddef.h>
    #include <string.h>
    #include "token.h"
#line 13 "keywords.gperf"
struct keyword {const char *name; tokenkind_t kind;};

#define TOTAL_KEYWORDS 21
#define MIN_WORD_LENGTH 2
#define MAX_WORD_LENGTH 9
#define MIN_HASH_VALUE 2
#define MAX_HASH_VALUE 26
/* maximum key range = 25, duplicates = 0 */

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
hash (str, len)
     register const char *str;
     register size_t len;
{
  static unsigned char asso_values[] =
    {
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 10,  5, 27,
      27, 20,  0, 27, 20,  5, 27, 27, 15, 15,
       0,  5, 27, 27, 10, 27, 10,  0, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27, 27, 27, 27, 27,
      27, 27, 27, 27, 27, 27
    };
  return len + asso_values[(unsigned char)str[1]];
}

struct keyword *
check_keyword (str, len)
     register const char *str;
     register size_t len;
{
  static struct keyword wordlist[] =
    {
#line 18 "keywords.gperf"
      {"if", if_keyword},
#line 15 "keywords.gperf"
      {"int", int_keyword},
#line 33 "keywords.gperf"
      {"null", null_keyword},
#line 27 "keywords.gperf"
      {"super", super_keyword},
#line 23 "keywords.gperf"
      {"public", public_keyword},
#line 21 "keywords.gperf"
      {"for", for_keyword},
#line 31 "keywords.gperf"
      {"interface", interface_keyword},
#line 26 "keywords.gperf"
      {"final", final_keyword},
#line 16 "keywords.gperf"
      {"double", double_keyword},
#line 17 "keywords.gperf"
      {"boolean", boolean_keyword},
#line 29 "keywords.gperf"
      {"abstract", abstract_keyword},
#line 34 "keywords.gperf"
      {"true", true_keyword},
#line 35 "keywords.gperf"
      {"false", false_keyword},
#line 25 "keywords.gperf"
      {"static", static_keyword},
#line 24 "keywords.gperf"
      {"private", private_keyword},
#line 19 "keywords.gperf"
      {"else", else_keyword,},
#line 30 "keywords.gperf"
      {"class", class_keyword},
#line 32 "keywords.gperf"
      {"import", import_keyword},
#line 28 "keywords.gperf"
      {"this", this_keyword},
#line 20 "keywords.gperf"
      {"while", while_keyword},
#line 22 "keywords.gperf"
      {"return", return_keyword}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register unsigned int key = hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= MIN_HASH_VALUE)
        {
          register struct keyword *resword;

          if (key < 14)
            {
              switch (key - 2)
                {
                  case 0:
                    resword = &wordlist[0];
                    goto compare;
                  case 1:
                    resword = &wordlist[1];
                    goto compare;
                  case 2:
                    resword = &wordlist[2];
                    goto compare;
                  case 3:
                    resword = &wordlist[3];
                    goto compare;
                  case 4:
                    resword = &wordlist[4];
                    goto compare;
                  case 6:
                    resword = &wordlist[5];
                    goto compare;
                  case 7:
                    resword = &wordlist[6];
                    goto compare;
                  case 8:
                    resword = &wordlist[7];
                    goto compare;
                  case 9:
                    resword = &wordlist[8];
                    goto compare;
                  case 10:
                    resword = &wordlist[9];
                    goto compare;
                  case 11:
                    resword = &wordlist[10];
                    goto compare;
                }
            }
          else
            {
              switch (key - 14)
                {
                  case 0:
                    resword = &wordlist[11];
                    goto compare;
                  case 1:
                    resword = &wordlist[12];
                    goto compare;
                  case 2:
                    resword = &wordlist[13];
                    goto compare;
                  case 3:
                    resword = &wordlist[14];
                    goto compare;
                  case 5:
                    resword = &wordlist[15];
                    goto compare;
                  case 6:
                    resword = &wordlist[16];
                    goto compare;
                  case 7:
                    resword = &wordlist[17];
                    goto compare;
                  case 10:
                    resword = &wordlist[18];
                    goto compare;
                  case 11:
                    resword = &wordlist[19];
                    goto compare;
                  case 12:
                    resword = &wordlist[20];
                    goto compare;
                }
            }
          return 0;
        compare:
          {
            register const char *s = resword->name;

            if (*str == *s && !strncmp (str + 1, s + 1, len - 1) && s[len] == '\0')
              return resword;
          }
        }
    }
  return 0;
}
