/*
    This file is part of the OpenCSA Project under the MIT License
*/
#ifndef OPENCSA_LEXER_H
#define OPENCSA_LEXER_H
#include "token.h"
#include <stdbool.h>

typedef struct lexer {
    char *file_path;

    char *ptr, *end_ptr;
    int file_length;
    bool failed;

    int line, col;
} lexer_t;

void lex_tok(lexer_t *lexer, token_t *tok);

bool lex_single_line_comment(lexer_t *lexer, token_t *tok);

bool lex_multi_line_comment(lexer_t *lexer, token_t *tok);

void lex_numeric_literal(lexer_t *lexer, token_t *tok);

void lex_double_literal(lexer_t *lexer, token_t *tok, int col, char *start);

void lex_identifier(lexer_t *lexer, token_t *tok);

void lex_string_literal(lexer_t *lexer, token_t *tok);

#endif