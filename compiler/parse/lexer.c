/*
    This file is part of the OpenCSA Project under the MIT License
*/

#include "lexer.h"
#include "token.h"

// Macro for checking whether a character is horizontal whitespace according to
// the Java standard;
#define is_horizontal_whitespace(c) (c == ' ' || c == '\t' || c == '\f')

// Gperf generated function for checking keywords
struct keyword {
    const char *name;
    tokenkind_t kind;
};

extern struct keyword *check_keyword(char *str, size_t len);

// Token mutator function definitions to resolve linker error
extern inline void tok_set_symbol(token_t *tok, tokenkind_t kind, int line,
                                  int col);
extern inline void tok_set_literal(token_t *tok, tokenkind_t kind, int line,
                                   int col, char *text, int len);

// This is the main Lexer routine
void lex_tok(lexer_t *lexer, token_t *tok) {
lexer_begin:

    // First, we must skip all horizontal whitespace
    while (is_horizontal_whitespace(lexer->ptr[0])) {
        ++lexer->ptr;
        ++lexer->col;
    }

    // This is the start of the lexer DFA
    switch (lexer->ptr[0]) {
    case 0:
        // Here, we must check for the end of the file
        if (lexer->ptr == lexer->end_ptr) {
            tok_set_symbol(tok, file_end, lexer->line, lexer->col);
            return;
        }

        // Otherwise, we can skip the character and restart the lexer
        ++lexer->ptr;
        ++lexer->col;
        goto lexer_begin;

    // Now, we will handle the new line characters
    case '\n':
        ++lexer->line;
        lexer->col = 1;
        ++lexer->ptr;
        goto lexer_begin;

    case '\r':
        ++lexer->line;
        lexer->col = 1;

        // According to the Java spec, /r/n is a single new line
        if (lexer->ptr[1] == '\n')
            lexer->ptr += 2;
        else
            ++lexer->ptr;

        goto lexer_begin;

    // Separators
    case '(':
        tok_set_symbol(tok, l_paren, tok->line, tok->col++);
        ++lexer->ptr;
        return;
    case ')':
        tok_set_symbol(tok, r_paren, tok->line, tok->col++);
        ++lexer->ptr;
        return;
    case '{':
        tok_set_symbol(tok, l_curly, tok->line, tok->col++);
        ++lexer->ptr;
        return;
    case '}':
        tok_set_symbol(tok, r_curly, tok->line, tok->col++);
        ++lexer->ptr;
        return;
    case '[':
        tok_set_symbol(tok, l_square, tok->line, tok->col++);
        ++lexer->ptr;
        return;
    case ']':
        tok_set_symbol(tok, r_square, tok->line, tok->col++);
        ++lexer->ptr;
        return;
    case ';':
        tok_set_symbol(tok, semicolon, tok->line, tok->col++);
        ++lexer->ptr;
        return;
    case '.':
        tok_set_symbol(tok, dot, tok->line, tok->col++);
        ++lexer->ptr;
        return;

    // Operators
    case '+':
        if (lexer->ptr[1] == '+') {
            tok_set_symbol(tok, plus_plus, lexer->line, lexer->col);
            lexer->ptr += 2;
            lexer->col += 2;
            return;
        }
        if (lexer->ptr[1] == '=') {
            tok_set_symbol(tok, plus_equals, lexer->line, lexer->col);
            lexer->ptr += 2;
            lexer->col += 2;
            return;
        }
        tok_set_symbol(tok, plus, lexer->line, lexer->col++);
        ++lexer->ptr;
        return;
    case '-':
        if (lexer->ptr[1] == '-') {
            tok_set_symbol(tok, minus_minus, lexer->line, lexer->col);
            lexer->ptr += 2;
            lexer->col += 2;
            return;
        }
        if (lexer->ptr[1] == '=') {
            tok_set_symbol(tok, minus_equals, lexer->line, lexer->col);
            lexer->ptr += 2;
            lexer->col += 2;
            return;
        }
        tok_set_symbol(tok, minus, lexer->line, lexer->col++);
        ++lexer->ptr;
        return;
    case '*':
        if (lexer->ptr[1] == '=') {
            tok_set_symbol(tok, asterisk_equals, lexer->line, lexer->col);
            lexer->ptr += 2;
            lexer->col += 2;
            return;
        }
        tok_set_symbol(tok, asterisk, lexer->line, lexer->col++);
        ++lexer->ptr;
        return;
    case '/':
        switch (lexer->ptr[1]) {
        case '/':
            if (lex_single_line_comment(lexer, tok))
                goto lexer_begin;
            return;
        case '*':
            if (lex_multi_line_comment(lexer, tok))
                goto lexer_begin;
        case '=':
            tok_set_symbol(tok, slash_equals, lexer->line, lexer->col);
            lexer->ptr += 2;
            lexer->col += 2;
            return;
        default:
            tok_set_symbol(tok, slash, lexer->line, lexer->col++);
            ++lexer->ptr;
            return;
        }
    case '%':
        if (lexer->ptr[1] == '=') {
            tok_set_symbol(tok, percent_equals, lexer->line, lexer->col);
            lexer->ptr += 2;
            lexer->col += 2;
            return;
        }
        tok_set_symbol(tok, percent, lexer->line, lexer->col++);
        ++lexer->ptr;
        return;
    case '=':
        if (lexer->ptr[1] == '=') {
            tok_set_symbol(tok, equals_equals, lexer->line, lexer->col);
            lexer->ptr += 2;
            lexer->col += 2;
            return;
        }
        tok_set_symbol(tok, equals, lexer->line, lexer->col++);
        ++lexer->ptr;
        return;
    case '!':
        if (lexer->ptr[1] == '=') {
            tok_set_symbol(tok, exclaimation_equals, lexer->line, lexer->col);
            lexer->ptr += 2;
            lexer->col += 2;
            return;
        }
        tok_set_symbol(tok, exclaimation, lexer->line, lexer->col++);
        ++lexer->ptr;
        return;
    case '>':
        if (lexer->ptr[1] == '=') {
            tok_set_symbol(tok, greater_equals, lexer->line, lexer->col);
            lexer->ptr += 2;
            lexer->col += 2;
            return;
        }
        tok_set_symbol(tok, greater, lexer->line, lexer->col++);
        ++lexer->ptr;
        return;
    case '<':
        if (lexer->ptr[1] == '=') {
            tok_set_symbol(tok, less_equals, lexer->line, lexer->col);
            lexer->ptr += 2;
            lexer->col += 2;
            return;
        }
        tok_set_symbol(tok, less, lexer->line, lexer->col++);
        ++lexer->ptr;
        return;
    case '&':
        tok_set_symbol(tok, ampersand_ampersand, lexer->line, lexer->col);
        if (lexer->ptr[1] != '&') {
            fprintf(stderr,
                    "error: %s: %d:%d: unknown symbol '&'. Did you mean '&&' "
                    "instead?\n",
                    lexer->file_path, lexer->line, lexer->col);
            lexer->failed = true;
            ++lexer->ptr;
            ++lexer->col;
        } else {
            lexer->ptr += 2;
            lexer->col += 2;
        }
        return;
    case '|':
        tok_set_symbol(tok, pipe_pipe, lexer->line, lexer->col);
        if (lexer->ptr[1] != '|') {
            fprintf(stderr,
                    "error: %s: %d:%d: unknown symbol '|'. Did you mean '||' "
                    "instead?\n",
                    lexer->file_path, lexer->line, lexer->col);
            lexer->failed = true;
            ++lexer->ptr;
            ++lexer->col;
        } else {
            lexer->ptr += 2;
            lexer->col += 2;
        }
        return;

    // Literals - we will begin with numeric literals.
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
        lex_numeric_literal(lexer, tok);
        return;

    case '"':
        lex_string_literal(lexer, tok);
        return;

    case 'a':
    case 'b':
    case 'c':
    case 'd':
    case 'e':
    case 'f':
    case 'g':
    case 'h':
    case 'i':
    case 'j':
    case 'k':
    case 'l':
    case 'm':
    case 'n':
    case 'o':
    case 'p':
    case 'q':
    case 'r':
    case 's':
    case 't':
    case 'u':
    case 'v':
    case 'w':
    case 'x':
    case 'y':
    case 'z':
    case 'A':
    case 'B':
    case 'C':
    case 'D':
    case 'E':
    case 'F':
    case 'G':
    case 'H':
    case 'I':
    case 'J':
    case 'K':
    case 'L':
    case 'M':
    case 'N':
    case 'O':
    case 'P':
    case 'Q':
    case 'R':
    case 'S':
    case 'T':
    case 'U':
    case 'V':
    case 'W':
    case 'X':
    case 'Y':
    case 'Z':
    case '_':
    case '$':
        lex_identifier(lexer, tok);
        return;

    default:
        // diagnose error for unexpected character
        fprintf(stderr, "error: %s: %d:%d:  unexpected character '%c'\n",
                lexer->file_path, lexer->line, lexer->col, lexer->ptr[0]);
        lexer->failed = true;
        // now we can drop this character and restart the lexer
        ++lexer->col;
        ++lexer->ptr;
        goto lexer_begin;
    }
}

bool lex_single_line_comment(lexer_t *lexer, token_t *tok) {
    // First, we need to move the pointer forward
    lexer->ptr += 2;
    lexer->col += 2;

    // Now, we must skip all characters until a newline.
    while (true) {
        // end of file is also acceptable
        switch (lexer->ptr[0]) {
        case 0:
            if (lexer->ptr == lexer->end_ptr) {
                tok_set_symbol(tok, file_end, lexer->line, lexer->col);
                return false;
            }
            ++lexer->ptr;
            ++lexer->col;
            continue;
        case '\n':
            ++lexer->line;
            lexer->col = 1;
            ++lexer->ptr;
            return true;
        case '\r':
            ++lexer->line;
            lexer->col = 1;
            if (lexer->ptr[1] == '\n')
                lexer->ptr += 2;
            else
                ++lexer->ptr;
            return true;
        default:
            ++lexer->col;
            ++lexer->ptr;
        }
    }
}

bool lex_multi_line_comment(lexer_t *lexer, token_t *tok) {
    // Again, we need to move the pointer forward
    lexer->ptr += 2;
    lexer->col += 2;

    while (true) {
        switch (lexer->ptr[0]) {
        case 0:
            if (lexer->ptr == lexer->end_ptr) {
                tok_set_symbol(tok, file_end, lexer->line, lexer->col);
                fprintf(stderr,
                        "error: %s: %d:%d: unexpected file end in multi line "
                        "comment\n",
                        lexer->file_path, lexer->line, lexer->col);
                lexer->failed = true;
                return false;
            }
            ++lexer->ptr;
            ++lexer->col;
            continue;
        case '\n':
            ++lexer->line;
            lexer->col = 1;
            ++lexer->ptr;
            continue;
        case '\r':
            ++lexer->line;
            lexer->col = 1;
            if (lexer->ptr[1] == '\n')
                lexer->ptr += 2;
            else
                ++lexer->ptr;
            continue;
        case '*':
            if (lexer->ptr[1] == '/') {
                // end of comment
                lexer->ptr += 2;
                lexer->col += 2;
                return true;
            }
            ++lexer->ptr;
            ++lexer->col;
            continue;
        default:
            ++lexer->ptr;
            ++lexer->col;
        }
    }
}

void lex_numeric_literal(lexer_t *lexer, token_t *tok) {
    int start_col = lexer->col++;
    char *start_ptr = lexer->ptr++;

    // We need to consume all digits
    while (true) {
        switch (lexer->ptr[0]) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            ++lexer->ptr;
            ++lexer->col;
            continue;
        case '.':
            lex_double_literal(lexer, tok, start_col, start_ptr);
            return;

        default:
            tok_set_literal(tok, int_literal, lexer->line, start_col, start_ptr,
                            lexer->ptr - start_ptr);
            return;
        }
    }
}

void lex_double_literal(lexer_t *lexer, token_t *tok, int col, char *ptr) {
    // consume the floating point
    ++lexer->ptr;
    ++lexer->col;

    // once again, we need to consume all digits
    while (true) {
        switch (lexer->ptr[0]) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            ++lexer->ptr;
            ++lexer->col;
            continue;
        default:
            tok_set_literal(tok, double_literal, lexer->line, col, ptr,
                            lexer->ptr - ptr);
            return;
        }
    }
}

void lex_identifier(lexer_t *lexer, token_t *tok) {
    // First, we must move the pointer to the current character.
    int start_col = lexer->col++;
    char *start_ptr = lexer->ptr++;

    // Now, we must consume all identifier characters
    while (true) {
        switch (lexer->ptr[0]) {
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
        case 'N':
        case 'O':
        case 'P':
        case 'Q':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'V':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
        case '_':
        case '$':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            ++lexer->ptr;
            ++lexer->col;
            continue;
        default: {
            // end of identifier
            // We need to check if this identifier is a keyword
            int len = lexer->ptr - start_ptr;
            // DO NOT free this memory - it is stack allocated
            struct keyword *kw = check_keyword(start_ptr, len);
            tok_set_literal(tok, kw ? kw->kind : identifier, lexer->line,
                            start_col, start_ptr, len);
            return;
        }
        }
    }
}

void lex_string_literal(lexer_t *lexer, token_t *tok) {
    // move the pointer ahead for the opening delimeter
    int start_col = lexer->col++;
    char *start_ptr = lexer->ptr++;

    while (true) {
        switch (lexer->ptr[0]) {
        case '"':
            // end of the string literal
            // we will omit the quotes from the string text
            tok_set_literal(tok, string_literal, lexer->line, start_col,
                            start_ptr + 1, lexer->ptr - start_ptr - 1);
            // We also need to consume the closing quote                            
            ++lexer->col;
            ++lexer->ptr;
            return;
        case 0:
            // unexpected EOF in literal
            if (lexer->ptr == lexer->end_ptr) {
                fprintf(stderr,
                        "error: %s: %d:%d: unexpected file end in string "
                        "literal. Did you forget the closing '\"'?\n",
                        lexer->file_path, lexer->line, lexer->col);
                lexer->failed = true;
                return;
            }
            ++lexer->ptr;
            ++lexer->col;
            continue;
        case '\\':
            // escape sequences
            if (lexer->ptr[1] == 'n' || lexer->ptr[1] == '\\' ||
                lexer->ptr[1] == '"') {
                lexer->ptr += 2;
                lexer->col += 2;
                continue;
            }
            fprintf(stderr,
                    "error: %s: %d:%d: invalid escape sequence '\\%c'\n",
                    lexer->file_path, lexer->line, lexer->col, lexer->ptr[1]);
            lexer->failed = true;
            // here we can just let it fall through
        default:
            ++lexer->ptr;
            ++lexer->col;
        }
    }
}