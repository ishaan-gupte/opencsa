# This file is part of the OpenCSA Project under the MIT License

set(CMAKE_C_STANDARD 17)

project(opencsa_compiler)

add_subdirectory(parse)
add_executable(javac main.c)

target_include_directories(javac PUBLIC
    "${CMAKE_SOURCE_DIR}/parse"
    "${CMAKE_BINARY_DIR}/parse"
)

target_link_libraries(javac PUBLIC 
    parse
)